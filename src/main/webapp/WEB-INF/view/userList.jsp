<!DOCTYPE html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>
        <spring:message code="title.list"/>
    </title>
</head>

<body>
<table style="text-align: center">
    <thead>
    <tr>
        <th>
            <spring:message code="column.name"/>
        </th>
        <th>
            <spring:message code="column.gender"/>
        </th>
        <th>
            <spring:message code="column.departmet"/>
        </th>
        <th>
            <spring:message code="column.weight"/>
        </th>
        <th>
            <spring:message code="column.agreed"/>
        </th>
        <th>
            <spring:message code="column.description"/>
        </th>
        <th>
            <spring:message code="column.option"/>
        </th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${userList}" var="user">
        <tr>
            <td>
                <c:out value="${user.name}"/>
            </td>
            <td>
                <c:out value="${user.genderId}"/>
            </td>
            <td>
                <c:out value="${user.department}"/>
            </td>
            <td>
                <c:out value="${user.weight}"/>
            </td>
            <td>
                <c:out value="${user.agreed}"/>
            </td>
            <td>
                <c:out value="${user.description}"/>
            </td>

            <td>
                <c:url value="/user" var="updateUserUrl">
                    <c:param value="${user.id}" name="id"/>
                </c:url>
                <a href="${updateUserUrl}">
                    <button type="submit">
                        <spring:message code="button.update"/>
                    </button>
                </a>
                <form:form action="/userDelete?id=${user.id}" method="post">
                    <button type="submit">
                        <spring:message code="button.delete"/>
                    </button>
                </form:form>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<c:url value="/user" var="newUserUrl"/>
<a href="${newUserUrl}">
    <button type="submit">
        <spring:message code="button.add"/>
    </button>
</a>
</body>
</html>

