<!DOCTYPE html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>
        <spring:message code="title.user"/>
    </title>
</head>

<body>
<form:form modelAttribute="user" action="/user" method="post">
    <form:hidden path="id"/>
    <br>

    <label>
        <spring:message code="label.name"/>
    </label>
    <form:input placeholder="Name" path="name"/>
    <br><br>

    <label>
        <spring:message code="label.gender"/>
    </label>
    <form:select path="genderId">
        <form:options items="${listOfGender}" itemValue="id" itemLabel="name"/>
    </form:select>
    <br><br>

    <label>
        <spring:message code="label.agreed"/>
    </label>
    <form:radiobuttons path="agreed" items="${agreedOption}"/>
    <br><br>

    <label>
        <spring:message code="label.dept"/>
    </label>
    <form:checkbox path="department" label="CSE" item="${department}"/>
    <br><br>

    <label>
        <spring:message code="label.weight"/>
    </label>
    <form:input placeholder="weight" path="weight"/>
    <br><br>

    <form:textarea placeholder="Describe Yourself" path="description"/>
    <br><br>

    <button type="submit">
        <spring:message code="${user.isNew() ? 'button.add' : 'button.update'}"/>
    </button>
</form:form>
</body>
</html>