package net.therap.controller;

import net.therap.model.Gender;
import net.therap.model.User;
import net.therap.service.UserService;
import net.therap.validator.UserEditor;
import net.therap.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.*;

/**
 * @author al.imran
 * @since 12/05/2021
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private UserValidator userValidator;

    private List<Gender> listOfGender;
    private Map<String, String> agreedOption;
    private boolean department;

    private void setupReferenceData(ModelMap modelMap, User user) {
        listOfGender = new ArrayList<>();
        agreedOption = new HashMap<>();
        department = false;

        listOfGender.add(new Gender(0, "-SELECT-"));
        listOfGender.add(new Gender(1, "Male"));
        listOfGender.add(new Gender(2, "Female"));

        agreedOption.put(messageSource.getMessage("key.yes", null, null),
                messageSource.getMessage("key.yes", null, null));

        agreedOption.put(messageSource.getMessage("key.no", null, null),
                messageSource.getMessage("key.no", null, null));

        modelMap.addAttribute("listOfGender", listOfGender);
        modelMap.addAttribute("agreedOption", agreedOption);
        modelMap.addAttribute("department", department);
        modelMap.addAttribute("user", user);
    }

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        dataBinder.addValidators(userValidator);
        dataBinder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        dataBinder.registerCustomEditor(Integer.TYPE, "gender", new UserEditor());
    }

    @GetMapping(value = "/userList")
    public String showTraineeRegistration(HttpSession httpSession,
                                          ModelMap modelMap) {

        modelMap.addAttribute("userList", userService.getAllUser());
        return "userList";
    }

    @GetMapping("/user")
    public String show(@RequestParam(value = "id", defaultValue = "0") int userId,
                       ModelMap modelMap,
                       HttpSession httpSession) {

        setupReferenceData(modelMap, userId == 0 ? new User() : userService.getUserById(userId));
        return "user";
    }

    @PostMapping(value = "/user")
    public String process(@Valid @ModelAttribute("user") User user,
                          BindingResult bindingResult,
                          HttpSession httpSession,
                          ModelMap modelMap,
                          SessionStatus sessionStatus) {

        if (bindingResult.hasErrors()) {
            setupReferenceData(modelMap, user);
            return "user";
        }

        userService.saveOrUpdate(user);

        sessionStatus.setComplete();
        return "redirect:/userList";
    }

    @PostMapping(value = "/userDelete")
    public String processDelete(@RequestParam("id") int userId) {
        userService.deleteUser(userId);
        return "redirect:/userList";
    }
}
