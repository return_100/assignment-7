package net.therap.model;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;

import static net.therap.util.StringConst.*;

/**
 * @author al.imran
 * @since 14/04/2021
 */
@Entity
@Table(name = "user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "seqUser", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqUser")
    private int id;

    @Column(name = "name")
    @NotNull
    @Size(min = 1, max = 100)
    @Pattern(regexp = NAME_REGEX)
    private String name;

    @Column(name = "gender_id", nullable = false)
    @NotNull
    private Integer genderId;

    @Column(name = "agreed", nullable = false)
    @NotNull
    @Size(min = 1, max = 30)
    private String agreed;

    @Column(name = "department", nullable = false)
    private boolean department;

    @Column(name = "weight", nullable = false)
    @NotNull
    @DecimalMin(value = "0.0", inclusive = false)
    @DecimalMax(value = "99.99", inclusive = true)
    private double weight;

    @Column(name = "description", nullable = false)
    @NotNull
    @Size(min = 1, max = 1000)
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGenderId() {
        return genderId;
    }

    public void setGenderId(Integer genderId) {
        this.genderId = genderId;
    }

    public String getAgreed() {
        return agreed;
    }

    public void setAgreed(String agreed) {
        this.agreed = agreed;
    }

    public boolean isDepartment() {
        return department;
    }

    public void setDepartment(boolean department) {
        this.department = department;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isNew() {
        return id == 0;
    }
}
