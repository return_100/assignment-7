package net.therap.model;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author al.imran
 * @since 24/05/2021
 */
public class Gender implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;

    private String name;

    public Gender(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return this.id;
    }

    @Override
    public boolean equals(Object o) {
        return (Objects.nonNull(o) &&
                ((Gender) o).getName().equals(name));
    }
}
