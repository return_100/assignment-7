package net.therap.validator;

import net.therap.model.Gender;
import org.springframework.stereotype.Service;

import java.beans.PropertyEditorSupport;
import java.util.Objects;

/**
 * @author al.imran
 * @since 24/05/2021
 */
@Service
public class UserEditor extends PropertyEditorSupport {

    @Override
    public String getAsText() {
        return String.valueOf(getValue());
    }

    @Override
    public void setAsText(String genderId) throws IllegalArgumentException {
        if (Objects.nonNull(genderId)) {
            int id = Integer.parseInt(genderId);

            Gender male = new Gender(1, "Male");
            Gender female = new Gender(2, "Female");

            setValue(male.getId() == id ? male : (female.getId() == id ? female : null));
        } else {
            setValue(null);
        }
    }
}
