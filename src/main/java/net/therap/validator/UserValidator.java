package net.therap.validator;

import net.therap.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author al.imran
 * @since 25/05/2021
 */
@Service
public class UserValidator implements Validator {

    @Autowired
    private MessageSource messageSource;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        if (errors.hasFieldErrors("name")) {
            errors.rejectValue("name", "user.name", messageSource.getMessage("message.invalidname", null, null));
        } else if (errors.hasFieldErrors("genderId")) {
            errors.rejectValue("genderId", "user.genderId", messageSource.getMessage("message.invalidgender", null, null));
        } else if (errors.hasFieldErrors("agreed")) {
            errors.rejectValue("agreed", "user.agreed", messageSource.getMessage("message.invalidagreed", null, null));
        } else if (errors.hasFieldErrors("weight")) {
            errors.rejectValue("weight", "user.weight", messageSource.getMessage("message.invalidweight", null, null));
        } else if (errors.hasFieldErrors("description")) {
            errors.rejectValue("description", "user.description", messageSource.getMessage("message.invaliddes", null, null));
        }
    }
}
