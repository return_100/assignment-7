package net.therap.dao;

import net.therap.model.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author al.imran
 * @since 14/04/2021
 */
@Repository
public class UserDao implements Dao<User> {

    @PersistenceContext
    private EntityManager em;

    private static final String JPQL_FIND_ALL = "FROM User";

    @Override
    public User findById(int id) {
        return em.find(User.class, id);
    }

    @Override
    public List<User> findAll() {
        return em.createQuery(JPQL_FIND_ALL, User.class).getResultList();
    }

    @Override
    @Transactional
    public void delete(int id) {
        User user = findById(id);
        em.remove(user);
    }

    @Override
    @Transactional
    public User saveOrUpdate(User user) {
        if (user.isNew()) {
            em.persist(user);
        } else {
            user = em.merge(user);
        }

        return user;
    }
}
