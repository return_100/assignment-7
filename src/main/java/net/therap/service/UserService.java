package net.therap.service;

import net.therap.dao.UserDao;
import net.therap.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * @author al.imran
 * @since 15/05/2021
 */
@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    public void saveOrUpdate(User user) {
        userDao.saveOrUpdate(user);
    }

    public List<User> getAllUser() {
        return userDao.findAll();
    }

    public User getUserById(int id) {
        return userDao.findById(id);
    }

    public void deleteUser(int id) {
        userDao.delete(id);
    }
}
