package net.therap.util;

/**
 * @author al.imran
 * @since 09/05/2021
 */
public interface StringConst {

    String NAME_REGEX = "[a-zA-Z]+[a-zA-Z0-9]*";
}
